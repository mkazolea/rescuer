cd output
xaxis
bed

% prompt = {'Give the number of the files'};
% titolo = '';
% dati = inputdlg(prompt,titolo,1);
% name = char(dati(1));
% nfiles=str2double(char(dati(1)));
% 
% for i=1:nfiles
%     if(i<10)
%         eval(['depth0' num2str(i)]);
%     else
%         eval(['depth0' num2str(i)]);
%     end
%     plot(x,b,x,eta,'linewidth',2);
%     pause(0.1)
% end
% depth
% plot(x,b,x,eta)
% 

%----
depth
porosity
%plot(x,por(:,1),x,eta,'linewidth',2);
%legend('\phi','h')
plot(x,primitives(:,2),'linewidth',2')
%axis([30 70 0 8.5])
axis([30 70 -3 6])
xlabel('$x(m) $','Fontsize',16,'Interpreter','latex')
ylabel('$velocity $','Fontsize',16,'Interpreter','latex')

s=struct('Version',1,'Format','pdf','Width','800','Height','200','Units','points','Resolution','300');
set(gcf,'Units','inches');
screenposition = get(gcf,'Position');
set(gcf,...
    'PaperPosition',[0 0 screenposition(3:4)],...
    'PaperSize',[screenposition(3:4)]);
str=['RPR_velocity_wb'];
print('-dpdf',str)