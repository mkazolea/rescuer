\documentclass{beamer}
\usepackage{amsmath, amssymb, graphicx, bm}
\def\red#1{{\color{red}#1}}
\def\r#1{{\color{brickRed}#1}}
\def\b#1{{\color{blue}#1}}
\def\g#1{{\color{green}#1}}
\def\y#1{{\color{yellow}#1}}
\definecolor{lg}{rgb}{0.1, 0.8, 0.5}
\definecolor{lb}{rgb}{0.06, 0.2, 0.6}



\newcommand{\cc}{\left(\frac{z_a^2}{2}-\frac{h^2}{6}\right)h}
\newcommand{\dd}{\left(z_a +\frac{h}{2}\right)h}
\newcommand{\f}{\frac{1}{2}}
\newcommand{\pr}{\partial}


\begin{document}
	
	% Slide 1: Title Slide
	\begin{frame}
		\centering
		\textbf{Overview of Porous Shallow Water Equations} \\ 
		Based on the Review by Dewals et al. \\ 
		\textit{For First-Year PhD Students}
	\end{frame}
	
	% Slide 2: Introduction
	\begin{frame}
		\frametitle{Urban flooding and modelling}
		\begin{itemize}
			\item Urban flooding is an increasing issue due to climate change and population growth.
			\item Traditional numerical models for flood simulations require high computational resources.
			\item \textbf{Porosity-based shallow water models} incorporate subgrid-scale effects, improving efficiency. Promising tool to account for the efects exerted by buildings, without impacting on the mesh resolution and hence on the computational times. 
			\item These models reduce computational time while maintaining accuracy in flood hazard prediction.	
		\end{itemize}
			\centering
	\includegraphics[width=0.35\textwidth]{figures/buildings.png} 		
	\end{frame}
	
	% Slide 3: Standard Shallow Water Equations
	\begin{frame}
		\frametitle{Standard Shallow Water Equations (SWE)}
		\begin{align*}
			\frac{\partial h}{\partial t} + \nabla \cdot (h \bm{u}) &= 0, \\
			\frac{\partial (h \bm{u})}{\partial t} + \nabla \cdot \left(h \bm{u} \otimes \bm{u} + \frac{1}{2} g h^2 \mathbb{I} \right) &= - g h \nabla b - \bm{R}.
		\end{align*}
		\begin{itemize}
			\item $h$: Water depth, $\bm{u}$: Velocity vector, $g$: Gravity acceleration, $b$: Bottom elevation.
			\item These equations assume a continuum medium, making them computationally expensive in urban settings.
		\end{itemize}
	\end{frame}
	
	% Slide 4: The Need for Porosity Models
	\begin{frame}
		\frametitle{Why Use Porosity Models?}
		\begin{itemize}
			\item Urban environments contain complex structures (buildings, streets, vegetation) that affect water flow.
			\item High-resolution numerical models explicitly resolving these structures are computationally prohibitive.
			\item Porosity models introduce effective parameters to account for subgrid-scale effects.
			\item They allow coarser grids while preserving critical flow dynamics in urban flood simulations.
		\end{itemize}
	Presence of an obstacle $ \rightarrow $ effect on the flow:
	\begin{itemize}
		\item \b{Reduce the volume available for water storage}
		\item Channelize the flow along directional pathways
		\item Induce flow resistance. 
	\end{itemize}
	\end{frame}
	
	% Slide 5: Control Volume and Porosity Parameters
	\begin{frame}
		\frametitle{Control Volume and Porosity Parameters}
		\begin{itemize}
			\item A control volume in porous shallow water models represents an area containing both water and solid obstacles.
			\item \b{\textbf{Storage porosity}} ($\phi$): Represents the fraction of the volume available for water storage: $\phi = V_{water}/V_{total}$ (statistical descriptor). {\tiny{ First introduced by Defina et. al (1994).}}
			\begin{itemize}
				\item The porosity is the same in all directions - the storage capacity does not vary with orientation.
				\item Often used in macroscopic flood models where fine-scale directional variations in porosity are ignored.
				\item Urban flooding-fraction of space available for water retention within a coarse-resolution cell, Natural porous media (soil,gravel beds)- represents the uniform capacity of the medium to retain water.
			\end{itemize}
			\item Several different isotropic formulations have been presented though the years, see for eg.\tiny{ Guinot et Frazao (2006), Cea and Vazquez-Cendon (2010), Ferrari et. al (2017), Viero (2019)}
	\end{itemize}
	\end{frame}
	
	
		% Slide 5a: Control Volume and Porosity Parameters
	\begin{frame}
		\frametitle{Control Volume and Porosity Parameters}
		\begin{itemize}
			\item \textbf{Conveyance porosity} ($\Psi$): Represents the fraction of space available for flow exchange.
			\item These parameters determine how water moves and is stored within the computational domain.
		\end{itemize}
	\vspace{0.5cm}
		\begin{itemize}
			\item Isotropic porosity assumes uniformity in all directions  
			\item Anisotropic porosity  means that storage capacity differs in different directions due to structural variations (e.g., buildings, terrain slopes, street orientations).
		\end{itemize}
	\end{frame}
	
	\begin{frame}
	\frametitle{Storage and Conveyance porosity}
	\tiny{
\begin{table}[h]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Property} & \textbf{Storage Porosity ($\phi$)} & \textbf{Conveyance Porosity ($\Psi$)} \\
		\hline
		\textbf{Definition} & Fraction of space available for water storage & Fraction of space available for flow movement \\
		\hline
		\textbf{Effect} & Affects water retention & Affects flow velocity and discharge \\
		\hline
		\textbf{Directionality} & Can be isotropic & Often anisotropic (direction-dependent) \\
		\hline
		\textbf{Influencing Factors} & Topography, obstacles & Flow pathways, obstacles, street orientation \\
		\hline
		\textbf{Urban Example} & Ponds, basements, open courtyards & Streets, alleys, channels \\
		\hline
	\end{tabular}
	\caption{Comparison of Storage Porosity ($\phi$) and Conveyance Porosity ($\Psi$)}
\end{table}
}
\end{frame}
	
	% Slide 6: Representative Elementary Volume (REV)
\begin{frame} 
		\frametitle{Representative Elementary Volume (REV)}
		\begin{itemize}
			\item The representative elementary volume (REV) is defined as the smallest control volume for which the statistical properties of the porous medium become independent of the size of this control volume.  
			\item A REV can be defined around any arbitrary point $(x,y)$, irrespective of the positioning of this specific point in water or an obstacle. 
		\end{itemize}
			\begin{figure}				

			\centering
			\includegraphics[width=0.35\textwidth]{figures/REV_models.png}\\
			\caption{{\tiny Picture taken from Dewals et al., Porosity models for large-scale Urban Flood Modelling: A Review, Water, 2021}  } 	
			\end{figure}
%		\begin{itemize}
%			\item The concept of REV is essential for defining porosity parameters.
%			\item REV is the smallest volume over which flow properties can be statistically averaged.
%			\item Used in porous shallow water models to approximate large-scale behavior of urban floods.
%			\item Helps in deriving effective equations for flow in urban settings.
%		\end{itemize}
	\end{frame}
	
	% Slide 7: Single Porosity Model (SPM)
	\begin{frame}
		\frametitle{Single Porosity Model (SPM)}
		\begin{itemize}
			\item First derived by Guinot and Soarez-Frazao (2006, by phase-averaging the standard SWE over a REV.
			\item Uses a single porosity parameter: $\phi = \Psi$. (Assumes that the available storage and conveyance space are identical).
			\item A uniform porosity value is assigned to the computational cells in the urban area.  
			\item Effective for large-scale modeling but lacks directional flow information.
			\item Less accurate in cases where anisotropy (directional flow resistance) is significant.
		\end{itemize}
	
	\begin{eqnarray*}
		\frac{\pr \phi h}{\pr t} +\frac{\pr \phi h u}{\pr x} &=& 0\\
		\frac{\pr \phi hu}{\pr t} +\frac{\pr}{\pr x} \left( \phi u^2 h + \frac{1}{2}g\phi h^2\right) &=& \frac{1}{2}gh\frac{\pr \phi}{\pr x} -g\phi h \frac{\pr b}{\pr x}
	\end{eqnarray*}
	\end{frame}
	
	% Slide 8: Multiple Porosity Model (MPM)
	\begin{frame}
		\frametitle{Multiple Porosity Model (MPM)}
		\begin{itemize}
			\item Introduces separate parameters for storage porosity ($\phi$) and conveyance porosity ($\Psi$).
			\item Accounts for preferential flow pathways in urban areas.
			\item More accurate in capturing directional flow effects compared to SPM.
			\item Commonly used in high-resolution flood modeling where directional flow plays a role.
		\end{itemize}
	\end{frame}
	
	% Slide 9: Integral Porosity Model (IPM)
	\begin{frame}
		\frametitle{Integral Porosity Model (IPM)}
		\begin{itemize}
			\item Developed using Reynolds Transport Theorem, leading to an \textit{ integral form }of the shallow water equations.
			\item Computes porosity parameters locally at the computational cell level. In these models, the complex structure of urban environments is represented by a binary indicator function $I(x,y)$, $I(x,y)=0$ in buildings and $I(x,y)=1$ in points of the domain that allow flood storage and conveyance.  
			\item Allows for anisotropic effects but is sensitive to mesh resolution and design.
			\item Works well with unstructured grids. 
		\end{itemize}
	\end{frame}
	
	% Slide 10: Dual Integral Porosity Model (DIPM)
	\begin{frame}
		\frametitle{Dual Integral Porosity Model (DIPM)}
		\begin{itemize}
			\item Enhances IPM by introducing separate treatment for control volumes and boundaries.
			\item Introduces a new momentum dissipation mechanism for transient waves.
			\item Captures complex anisotropic flow effects better than previous models.
			\item Reduces sensitivity to computational mesh design.
		\end{itemize}
	\end{frame}
	
		\begin{frame}
		\frametitle{Binary Single Porosity Models (BSPM)}
		\begin{itemize}
			\item Derived very recently by Varra et al. 2020,2024.
			\item Theoretical connection between IPM and SPM.
			\item Allow for local definition of the urban fabric geometric characteristics. The BSP model inherits the structure of the SWE by replacing porosity 
			$\phi(x,y)$ with a binary function.
		
		\end{itemize}
	\end{frame}
	
	
	
% Slide 13: Applications of Porous SWEs
\begin{frame}
	\frametitle{Applications in Flood Modeling}
	\begin{itemize}
		\item Used in urban flood prediction and disaster response planning.
		\item Applied in coastal flood impact analysis.
		\item Essential for large-scale hydrodynamic simulations with complex topography.
	\end{itemize}
	\centering
%	\includegraphics[width=0.7\textwidth]{flood_application.png} 
%	\caption{Application of porous SWEs in urban flood modeling.}
\end{frame}

% Slide 14: Case Studies
\begin{frame}
	\frametitle{Case Studies in Urban Hydrodynamics}
	\begin{itemize}
		\item Simulation of flood events in European cities.
		\item Validation against real-world flood data.
		\item Model performance comparison with traditional SWEs.
	\end{itemize}
	\centering
%	\includegraphics[width=0.7\textwidth]{case_study.png} 
%	\caption{Real-world case study applying porosity models.}
\end{frame}

% Slide 15: Challenges and Limitations
\begin{frame}
	\frametitle{Challenges in Porous SWEs}
	\begin{itemize}
		\item Calibration of porosity parameters for different urban environments.
		\item Computational efficiency vs. accuracy trade-off.
		\item Sensitivity to boundary conditions and numerical discretization.
	\end{itemize}
\end{frame}

	
	% Slide 20: Conclusion
	\begin{frame}
		\frametitle{We will work with}
		\begin{itemize}
%			\item Porosity-based shallow water models provide computationally efficient flood modeling approaches.
%			\item Different formulations exist, with trade-offs in accuracy and computational cost.
%			\item Future research is needed in model validation, parameter calibration, and real-world implementation.
		\item A single porosity model (SP) - Porosity as a statistical descriptor 
		\item Depth-independent porosity
		\item Model expressed in differential form
		\item Isotropic porosity effects
		\item Shock-capturing scheme 
		\end{itemize}
	\end{frame}
	
\end{document}
 